Desempenho::Application.routes.draw do
  
  resources :employments
  devise_for :users, :path => 'usuarios', :path_names => {:sign_in => 'logar', :sign_out => 'sair', :sign_up => 'cadastrar'}
  root :to => "home#index"
  

  resources :surveys do
  resources :survey
  end

  resources :employees do
  resources :employee
  end

  resources :competences do
  resources :competence
  end

  resources :employments do
  resources :employment
  end

  resources :departaments do
  resources :departament
  end
  resources :questions do
  resources :question
  end

  resources :answers do
  resources :answer
  end

end
