class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|  
      t.string :name
      t.integer :identify
      t.references :departament
      t.references :employment
      
      t.timestamps
    end
  end
end
