class CreateCompetences < ActiveRecord::Migration
  def change
    create_table :competences do |t|  
      t.string :name
      t.boolean :enabled
      t.references :question     
      t.timestamps
    end
  end
end
