class CreateEmployments < ActiveRecord::Migration
  def change
    create_table :employments do |t|  
      t.string :name
      t.references :competence
      t.timestamps
    end
  end
end
