class AddQuestionsAndCompetences < ActiveRecord::Migration
  def self.up
    create_table :competences_questions do |t|
      t.references :competences, :questions
    end
  end
 
  def self.down
    drop_table :competences_questions
  end
end