class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|  
      t.references :employee
      t.references :departament
      t.references :employment
      t.integer :product
      
      t.timestamps
    end
  end
end
