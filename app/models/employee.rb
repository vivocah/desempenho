class Employee < ActiveRecord::Base
  attr_accessible :name, :identify, :departament_id, :employment_id
  belongs_to :departament
  belongs_to :employment
  has_many :surveys
end
