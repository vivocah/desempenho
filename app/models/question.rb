class Question < ActiveRecord::Base
  attr_accessible :name
  attr_accessible :answers_attributes
  has_many :answers, :dependent => :destroy
  has_and_belongs_to_many :competences
  accepts_nested_attributes_for :answers, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
end

