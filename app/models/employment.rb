class Employment < ActiveRecord::Base
  attr_accessible :name, :competence_id
  belongs_to :competence
  has_many :employees
  has_many :surveys
end
