class Answer < ActiveRecord::Base
  attr_accessible :name, :score
  belongs_to :question
end
