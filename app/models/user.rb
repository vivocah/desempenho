class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable,
    :token_authenticatable, :timeoutable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :login, :enabled, :url_image,:email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
validates_presence_of :login
end