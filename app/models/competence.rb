class Competence < ActiveRecord::Base
  attr_accessible :name, :enabled
  attr_accessible :questions_attributes
  has_many :employments
  has_and_belongs_to_many :questions
  accepts_nested_attributes_for :questions, :reject_if => :all_blank, :allow_destroy => true

  # Competence.named("Surf")
  #scope :named, lambda {|param| where('name LIKE ?', "%#{param}%") }

  # Competence.named("Surf")
  def self.named(param)
  	where('name LIKE ?', "%#{param}%")
  end

  # c = Competence.last; c.last_question.answers
  def last_question
  	questions.last
  end
end
