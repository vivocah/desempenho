class Survey < ActiveRecord::Base
  attr_accessible :employee_id, :departament_id, :employment_id, :product
  belongs_to :employee
  belongs_to :departament
  belongs_to :employment
end
