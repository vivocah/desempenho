class SurveysController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @surveys = Survey.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @surveys
  end

  def show
    @survey = get_register(params[:id])
    respond_with @survey
  end

  def new
    @survey = Survey.new
    respond_with @survey
  end

  def edit
    @survey = get_register(params[:id])
    respond_with @survey
  end

  def create
    @survey = Survey.new params[:survey]
    flash[:notice] = t :survey_created if @survey.save
    respond_with @survey
  end

  def update
    @survey = get_register(params[:id])
    flash[:notice] = t :survey_updated if @survey.update_attributes params[:survey]
    respond_with @survey
  end

  def destroy
    @survey = get_register(params[:id])
    @survey.destroy
    respond_with @survey
  end
  def teste
    begin
      @employee = Employee.find(params[:id])
      @employment = @employee.employment
      @competence = @employment.competences
      @competences = @employment.competences 
      rescue
      redirect_to employee_path
    end
   end


  private
  def get_register(id)
    Survey.find(id)
  end

end