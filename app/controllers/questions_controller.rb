class QuestionsController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @questions = Question.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @questions
  end

  def show
    @question = get_register(params[:id])
    respond_with @question
  end

  def new
   @question = Question.new
   5.times { @question.answers.build }
   respond_with @question
  end

  def edit
    @question = get_register(params[:id])
    respond_with @question
  end

  def create
   @question = Question.new params[:question]
   flash[:notice] = t :question_created if @question.save
   respond_with @question
  end

  def update
    @question = get_register(params[:id])
    flash[:notice] = t :question_updated if @question.update_attributes params[:question]
    respond_with @question
  end

  def destroy
    @question = get_register(params[:id])
    @question.destroy
    respond_with @question
  end

  private
  def get_register(id)
    Question.find(id)
  end

end
