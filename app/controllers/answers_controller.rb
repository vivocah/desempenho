class AnswersController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @answers = Answer.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @answers
  end

  def show
    @answer = get_register(params[:id])
    respond_with @answer
  end

  def new
    @answer = Answer.new
    respond_with @answer
  end

  def edit
    @answer = get_register(params[:id])
    respond_with @answer
  end

  def create
    @answer = Answer.new params[:answer]
    flash[:notice] = t :answer_created if @answer.save
    respond_with @answer
  end

  def update
    @answer = get_register(params[:id])
    flash[:notice] = t :answer_updated if @answer.update_attributes params[:answer]
    respond_with @answer
  end

  def destroy
    @answer = get_register(params[:id])
    @answer.destroy
    respond_with @answer
  end

  private
  def get_register(id)
    Answer.find(id)
  end

end
