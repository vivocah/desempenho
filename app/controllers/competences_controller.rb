class CompetencesController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @competences = Competence.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @competences
  end

  def show
    @competence = get_register(params[:id])
    respond_with @competence 
    
  end

  def new
    @competence = Competence.new
    question = @competence.questions.build
    5.times { question.answers.build }
    respond_with @competence
  end

  def edit
    @competence = get_register(params[:id])
    respond_with @competence
  end

  def create
    @competence = Competence.new params[:competence]

    flash[:notice] = t :competence_created if @competence.save
    respond_with @competence
  end

  def update
    @competence = get_register(params[:id])

    flash[:notice] = t :competence_updated if @competence.update_attributes params[:competence]
    respond_with @competence
  end

  def destroy
    @competence = get_register(params[:id])
    @competence.destroy

    respond_with @competence
  end

  private
  def get_register(id)
    Competence.find(id)
  end

end
