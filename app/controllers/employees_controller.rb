class EmployeesController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @employees = Employee.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @employees
  end

  def show
    @employee = get_register(params[:id])
    respond_with @employee
  end

  def new
    @employee = Employee.new
    respond_with @employee
  end

  def edit
    @employee = get_register(params[:id])
    respond_with @employee
  end

  def create
    @employee = Employee.new params[:employee]
    flash[:notice] = t :employee_created if @employee.save
    respond_with @employee
  end

  def update
    @employee = get_register(params[:id])
    flash[:notice] = t :employee_updated if @employee.update_attributes params[:employee]
    respond_with @employee
  end

  def destroy
    @employee = get_register(params[:id])
    @employee.destroy
    respond_with @employee
  end

  private
  def get_register(id)
    Employee.find(id)
  end

end
