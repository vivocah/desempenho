class DepartamentsController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @departaments = Departament.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @departaments
  end

  def show
    @departament = get_register(params[:id])
    respond_with @departament
  end

  def new
    @departament = Departament.new
    respond_with @departament
  end

  def edit
    @departament = get_register(params[:id])
    respond_with @departament
  end

  def create
    @departament = Departament.new params[:departament]
    flash[:notice] = t :departament_created if @departament.save
    respond_with @departament
  end

  def update
    @departament = get_register(params[:id])
    flash[:notice] = t :departament_updated if @departament.update_attributes params[:departament]
    respond_with @departament
  end

  def destroy
    @departament = get_register(params[:id])
    @departament.destroy
    respond_with @departament
  end

  private
  def get_register(id)
    Departament.find(id)
  end

end
