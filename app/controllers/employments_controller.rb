class EmploymentsController < ApplicationController

  layout 'admin'
  respond_to :html, :json

  def index
    @employments = Employment.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_with @employments
  end

  def show
    @employment = get_register(params[:id])
    respond_with @employment
  end

  def new
    @employment = Employment.new
    respond_with @employment
  end

  def edit
    @employment = get_register(params[:id])
    respond_with @employment
  end

  def create
    @employment = Employment.new params[:employment]
    if @employment.save
      flash[:notice] = t :employment_created if @employment.save
      respond_with @employment
    end
  end

  def update
    @employment = get_register(params[:id])
    flash[:notice] = t :employment_updated if @employment.update_attributes params[:employment]
    respond_with @employment
  end

  def destroy
    @employment = get_register(params[:id])
    @employment.destroy
    respond_with @employment
  end

  private
  def get_register(id)
    Employment.find(id)
  end

end
